/***************************************************************
 * Name:      _020_12_11_FirstApp.cpp
 * Purpose:   Code for Application Class
 * Author:    Rachel (rachel@moosader.com)
 * Created:   2020-12-11
 * Copyright: Rachel (moosadee.com)
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "_020_12_11_FirstApp.h"
#include "_020_12_11_FirstMain.h"

IMPLEMENT_APP(_020_12_11_FirstApp);

bool _020_12_11_FirstApp::OnInit()
{
    _020_12_11_FirstFrame* frame = new _020_12_11_FirstFrame(0L);
    
    frame->Show();
    
    return true;
}
