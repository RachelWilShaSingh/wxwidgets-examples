/***************************************************************
 * Name:      _020_12_11_FirstMain.h
 * Purpose:   Defines Application Frame
 * Author:    Rachel (rachel@moosader.com)
 * Created:   2020-12-11
 * Copyright: Rachel (moosadee.com)
 * License:
 **************************************************************/

#ifndef _020_12_11_FIRSTMAIN_H
#define _020_12_11_FIRSTMAIN_H



#include "_020_12_11_FirstApp.h"


#include "GUIFrame.h"

class _020_12_11_FirstFrame: public GUIFrame
{
public:
    _020_12_11_FirstFrame(wxFrame *frame);
    ~_020_12_11_FirstFrame();
private:
    virtual void OnClose(wxCloseEvent& event);
    virtual void OnQuit(wxCommandEvent& event);
    virtual void OnAbout(wxCommandEvent& event);

    // Using this tutorial: https://wiki.wxwidgets.org/Writing_Your_First_Application-Using_The_WxTextCtrl
    wxTextCtrl* m_textbox;
};

// Control IDs and stuff
enum
{
    TEXT_textbox
};

#endif // _020_12_11_FIRSTMAIN_H
