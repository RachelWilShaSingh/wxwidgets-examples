/***************************************************************
 * Name:      _020_12_11_FirstApp.h
 * Purpose:   Defines Application Class
 * Author:    Rachel (rachel@moosader.com)
 * Created:   2020-12-11
 * Copyright: Rachel (moosadee.com)
 * License:
 **************************************************************/

#ifndef _020_12_11_FIRSTAPP_H
#define _020_12_11_FIRSTAPP_H

#include <wx/app.h>

class _020_12_11_FirstApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // _020_12_11_FIRSTAPP_H
