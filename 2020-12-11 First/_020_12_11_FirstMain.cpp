/***************************************************************
 * Name:      _020_12_11_FirstMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    Rachel (rachel@moosader.com)
 * Created:   2020-12-11
 * Copyright: Rachel (moosadee.com)
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "_020_12_11_FirstMain.h"

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__WXMAC__)
        wxbuild << _T("-Mac");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}


_020_12_11_FirstFrame::_020_12_11_FirstFrame(wxFrame *frame)
    : GUIFrame(frame)
{
#if wxUSE_STATUSBAR
    statusBar->SetStatusText(_("Hello Code::Blocks user!"), 0);
    statusBar->SetStatusText(wxbuildinfo(short_f), 1);
#endif

    // parent, id, value, post, size, style, validator, name
    m_textbox = new wxTextCtrl(
        this,
        TEXT_textbox,
        wxT( "Poop" ),
        wxDefaultPosition,
        wxSize( 200, 50 ), //wxDefaultSize,
        wxTE_MULTILINE | wxTE_RICH,
        wxDefaultValidator,
        wxTextCtrlNameStr
    );
}

_020_12_11_FirstFrame::~_020_12_11_FirstFrame()
{
}

void _020_12_11_FirstFrame::OnClose(wxCloseEvent &event)
{
    Destroy();
}

void _020_12_11_FirstFrame::OnQuit(wxCommandEvent &event)
{
    Destroy();
}

void _020_12_11_FirstFrame::OnAbout(wxCommandEvent &event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}
